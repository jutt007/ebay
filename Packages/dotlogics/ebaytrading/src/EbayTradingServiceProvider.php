<?php

namespace dotlogics\ebaytrading;

use Illuminate\Support\ServiceProvider;

class EbayTradingServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        $this->mergeConfigFrom(__DIR__.'/config/ebaytrading.php','ebaytrading');
        
        $this->publishes([
            __DIR__.'/config/ebaytrading.php' => config_path('ebaytrading.php')
        ]);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->bind('ebaytrading', function()
        {
            return new Controllers\EbayTradingController();
        });
    }
}
