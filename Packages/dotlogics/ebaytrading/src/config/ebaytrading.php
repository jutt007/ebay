<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Development mode?
    |--------------------------------------------------------------------------
    | True = Sandbox environment, False = Production environment.
    */
    'sandbox_mode' => true,

    /*
    |--------------------------------------------------------------------------
    | Site ID the eBay site id. See http://developer.ebay.com/devzone/finding/Concepts/SiteIDToGlobalID.html
    |--------------------------------------------------------------------------
    | Required do not remove.
    */
    'site_id' => 0,

    /*
    |--------------------------------------------------------------------------
    | The Ebay sandbox credentials
    |--------------------------------------------------------------------------
    | All fields required do not remove.
    */

    'sandbox_app_id' => 'Your Sandbox App ID',
    'sandbox_dev_id' => 'Your Sandbox Dev ID',
    'sandbox_cert_id' => 'Your Sandbox Cert ID',
    'sandbox_runame' => 'Your Sandbox RU_Name',

    /*
    |--------------------------------------------------------------------------
    | The Ebay production credentials
    |--------------------------------------------------------------------------
    | All fields required do not remove.
    */

    'live_app_id' => 'Your Live App ID',
    'live_dev_id' => 'Your Live Dev ID',
    'live_cert_id' => 'Your Live Cert ID',
    'live_runame' => 'Your Live RU_Name',

];