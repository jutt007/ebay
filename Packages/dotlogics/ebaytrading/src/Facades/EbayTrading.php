<?php

namespace dotlogics\ebaytrading\Facades;

use Illuminate\Support\Facades\Facade;

class EbayTrading extends Facade{

	protected static function getFacadeAccessor(){
		return 'ebaytrading';
	}
}